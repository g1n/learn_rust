fn main() {
   let mut x = 5;
   println!("The value of x is: {}", x);
   x = 6;
   println!("Now x is: {}", x);
}
